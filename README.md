Fourhour_broadcaster api reference
==================================


Method
------

You can call api functions defined by fourhour_broadcaster.
In following examples, `fourhour` is the id of broadcaster dom element.


### **getCameraNames** ###

returns camera name array

#### parameter ####
none
#### example ####
    fourhour.getCameraNames();


### **selectCamera** ###

if you want to detach camera input, put -1 into the parameter

#### parameter ####
index: index(int) of camera to select
#### example ####
    fourhour.selectCamera(-1);


### **selectCameraByName** ###

#### parameter ####
cameraName: name(string) of camera to select
#### example ####
    fourhour.selectCameraByName('camera_name');


### **getMicrophoneNames** ###

returns microphone name array

#### parameter ####
none
#### example ####
    fourhour.getMicrophoneNames();


### **selectMicrophone** ###

if you want to detach microphone input, put -1 into the parameter

#### parameter ####
index: index(int) of microphone to select
#### example ####
    fourhour.selectMicrophone(-1);


### **selectMicrophoneByName** ###

#### parameter ####
microphoneName: name(string) of microphone to select
#### example ####
    fourhour.selectMicrophoneByName('microphone_name');


### **getQuality** ###

returns [0-100] int value

#### parameter ####
none
#### example ####
    fourhour.getQuality();


### **setQuality** ###

#### parameter ####
value: quality int value [0-100]
#### example ####
    fourhour.setQuality(50);


### **setCameraMode** ###

#### parameter ####
width: int value
height: int value
fps: framerate float value
#### example ####
    fourhour.setCameraMode(640, 480, 30);


### **getCameraWidth** ###

returns camera width int value

#### parameter ####
none
#### example ####
    fourhour.getCameraWidth();


### **setCameraWidth** ###

#### parameter ####
value: camera width int value
#### example ####
    fourhour.setCameraWidth(640);


### **getCameraHeight** ###

returns camera height int value

#### parameter ####
none
#### example ####
    fourhour.getCameraHeight();


### **setCameraHeight** ###

#### parameter ####
value: camera height int value
#### example ####
    fourhour.setCameraWidth(480);


### **getCameraFps** ###

returns camera framerate float value

#### parameter ####
none
#### example ####
    fourhour.getCameraFps();


### **setCameraFps** ###

#### parameter ####
value: camera framerate float value
#### example ####
    fourhour.setCameraFps(30);


### **getVolume** ###

returns [0-100] int value

#### parameter ####
none
#### example ####
    fourhour.getVolume();


### **setVolume** ###

#### parameter ####
value: volume int value [0-100]
#### example ####
    fourhour.setVolume(50);


### **getMicrophoneRate** ###

The rate at which the microphone is capturing sound, in kHz.
The default value is 8 kHz if your sound capture device supports this value.
Otherwise, the default value is the next available capture level above 8 kHz
that your sound capture device supports, usually 11 kHz.

returns 5 | 8 | 11 | 22 | 44 int value

#### parameter ####
none
#### example ####
    fourhour.getMicrophoneRate();


### **setMicrophoneRate** ###

The rate at which the microphone is capturing sound, in kHz.
Acceptable values are 5, 8, 11, 22, and 44.

#### parameter ####
value: microphone rate(kHz) 5 | 8 | 11 | 22 | 44 int value
#### example ####
    fourhour.setMicrophoneRate(44);


### **startBroadcast** ###

#### parameter ####
address: address string

channel: channel string

room: room string
#### example ####
    fourhour.startBroadcast('127.0.0.1', 'channel_name', 'room_name');


### **stopBroadcast** ###

#### parameter ####
none
#### example ####
    fourhour.stopBroadcast();


### **getConnected** ###

returns bool value

#### parameter ####
none
#### example ####
    fourhour.getVolume();



Callback
--------

Fourhour_broadcaster calls javascript's function
when specific event occured


### **onInit** ###

called when fourhour api is readied

#### example ####
    function onInit() {
        console.log('api initialized');
    }


### **onConnect** ###

called when connection succeed

#### parameter ####
debugMsg: debug message string
#### example ####
    function onConnect() {
        console.log(debugMsg);
    }


### **onError** ###

called when connection failed

#### parameter ####
debugMsg: debug message string
#### example ####
    function onError() {
        console.log(debugMsg);
    }