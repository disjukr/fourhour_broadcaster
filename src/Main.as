package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.external.ExternalInterface;
	import flash.media.Camera;
	import flash.media.Microphone;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.Security;
	import flash.text.TextField;
	
	public class Main extends Sprite
	{
		private var camera:Camera;
		private var microphone:Microphone;
		
		private var connection:NetConnection;
		private var stream:NetStream;
		private var room:String;
		
		private var display:Video;
		private var traceDisplay:TextField;
		
		public function Main():void
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			Security.allowDomain("*");
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, RESIZE);
			
			display = stage.addChild(new Video(stage.stageWidth, stage.stageHeight)) as Video;
			selectCamera();
			selectMicrophone();
			
			traceDisplay = stage.addChild(new TextField) as TextField;
			traceDisplay.width = stage.stageWidth;
			traceDisplay.height = stage.stageHeight;
			traceDisplay.mouseEnabled = false;
			
			if (ExternalInterface.available)
			{
				ExternalInterface.addCallback("getCameraNames", getCameraNames);
				ExternalInterface.addCallback("selectCamera", selectCamera);
				ExternalInterface.addCallback("selectCameraByName", selectCameraByName);
				ExternalInterface.addCallback("getMicrophoneNames", getMicrophoneNames);
				ExternalInterface.addCallback("selectMicrophone", selectMicrophone);
				ExternalInterface.addCallback("selectMicrophoneByName", selectMicrophoneByName);
				ExternalInterface.addCallback("getQuality", getQuality);
				ExternalInterface.addCallback("setQuality", setQuality);
				ExternalInterface.addCallback("setCameraMode", setCameraMode);
				ExternalInterface.addCallback("getCameraWidth", getCameraWidth);
				ExternalInterface.addCallback("setCameraWidth", setCameraWidth);
				ExternalInterface.addCallback("getCameraHeight", getCameraHeight);
				ExternalInterface.addCallback("setCameraHeight", setCameraHeight);
				ExternalInterface.addCallback("getCameraFps", getCameraFps);
				ExternalInterface.addCallback("setCameraFps", setCameraFps);
				ExternalInterface.addCallback("getVolume", getVolume);
				ExternalInterface.addCallback("setVolume", setVolume);
				ExternalInterface.addCallback("getMicrophoneRate", getMicrophoneRate);
				ExternalInterface.addCallback("setMicrophoneRate", setMicrophoneRate);
				ExternalInterface.addCallback("startBroadcast", startBroadcast);
				ExternalInterface.addCallback("stopBroadcast", stopBroadcast);
				ExternalInterface.addCallback("getConnected", getConnected);
				ExternalInterface.call("onInit");
			}
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function log(text:String):void
		{
			if (traceDisplay != null)
				traceDisplay.appendText(text += "\n");
		}
		
		private function RESIZE(e:Event):void
		{
			display.width = stage.stageWidth;
			display.height = stage.stageHeight;
			traceDisplay.width = stage.stageWidth;
			traceDisplay.height = stage.stageHeight;
		}
		
		private function NET_STATUS(e:NetStatusEvent):void
		{
			function jsEvent(callback:String):void
			{
				if (ExternalInterface.available)
					ExternalInterface.call(callback, e.info.code);
			}
			if (e.info.level == "error")
				jsEvent("onError");
			switch (e.info.code)
			{
				case "NetConnection.Connect.Success":
					stream = new NetStream(connection);
					stream.attachCamera(camera);
					stream.attachAudio(microphone);
					stream.publish(room, "live");
					jsEvent("onConnect");
					break;
				case "NetConnection.Connect.Closed":
					jsEvent("onError");
					break;
			}
		}
		
		private function getCameraNames():Array
		{
			return Camera.names;
		}
		
		private function selectCamera(index:int = -1):void
		{
			camera = Camera.getCamera((index == -1)? null : String(index));
			if (camera != null)
			{
				camera.setLoopback(true);
				display.attachCamera(camera);
				setCameraMode(640, 480, 30);
				setQuality(50);
			}
		}
		
		private function selectCameraByName(cameraName:String = null):void
		{
			var index:int = Camera.names.indexOf(cameraName);
			selectCamera(index);
		}
		
		private function getMicrophoneNames():Array
		{
			return Microphone.names;
		}
		
		private function selectMicrophone(index:int = -1):void
		{
			microphone = Microphone.getMicrophone(index);
			setVolume(50);
			if (microphone != null)
				microphone.enableVAD = false;
		}
		
		private function selectMicrophoneByName(microphoneName:String = null):void
		{
			var index:int = Microphone.names.indexOf(microphoneName);
			selectMicrophone(index);
		}
		
		private function getQuality():int
		{
			return (camera != null)? camera.quality : 0;
		}
		
		private function setQuality(value:int):void
		{
			if (camera)
				camera.setQuality(0, value);
		}
		
		private function setCameraMode(width:int, height:int, fps:Number):void
		{
			if (camera)
				camera.setMode(width, height, fps);
		}
		
		private function getCameraWidth():int
		{
			return (camera != null)? camera.width : 0;
		}
		
		private function setCameraWidth(value:int):void
		{
			if (camera)
				setCameraMode(value, camera.height, camera.fps);
		}
		
		private function getCameraHeight():int
		{
			return (camera != null)? camera.height : 0;
		}
		
		private function setCameraHeight(value:int):void
		{
			if (camera)
				setCameraMode(camera.width, value, camera.fps);
		}
		
		private function getCameraFps():Number
		{
			return (camera != null)? camera.fps : 0;
		}
		
		private function setCameraFps(value:Number):void
		{
			if (camera)
				setCameraMode(camera.width, camera.height, value);
		}
		
		private function getVolume():int
		{
			return (microphone != null)? microphone.gain : 0;
		}
		
		private function setVolume(value:int):void
		{
			if (microphone)
				microphone.gain = value;
		}
		
		private function getMicrophoneRate():int
		{
			return (microphone != null)? microphone.rate : 0;
		}
		
		public function setMicrophoneRate(value:int):void
		{
			if (microphone)
				microphone.rate = value;
		}
		
		private function startBroadcast(address:String, channel:String, room:String):void
		{
			if (connection) connection.removeEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
			connection = new NetConnection;
			connection.addEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
			connection.connect("rtmp://" + address + "/" + channel);
			this.room = room;
		}
		
		private function stopBroadcast():void
		{
			if (connection) connection.removeEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
			if (getConnected()) connection.close();
			if (stream)
			{
				stream.attachCamera(null);
				stream.attachAudio(null);
				stream.close();
			}
			connection = null;
			stream = null;
		}
		
		private function getConnected():Boolean
		{
			return connection && connection.connected;
		}
	}
}